const userLanguage = navigator.language;

const formatCurrency = (numString) => {
  return new Intl.NumberFormat(userLanguage, {
    style: "currency",
    currency: "USD",
  }).format(parseFloat(numString));
};

const formatNumber = (numString) => {
  return new Intl.NumberFormat(userLanguage).format(parseFloat(numString));
};

export const CardOne = (props) => {
  return (
    <div className={`backdrop-blur-lg rounded-3xl ring-2 ring-white/10 bg-gradient-to-br from-white/20 to-white/5 max-w-[300px] sm:max-w-full shadow-xl shadow-black/20 ${props.className}`}>
      <div className="grid grid-cols-2 p-5 gap-4">
        <div className="col-span-2 justify-self-start">
          <div className="rounded-md bg-white/10 shadow-md flex items-center py-1.5 px-1 leading-none gap-1">
            <div className={`h-3 w-3 rounded-full  ${props.active ? ' bg-green-300' : 'bg-red-400'}`}></div>
            <p className="text-white capitalize text-base">active</p>
          </div>
        </div>
        <div>
          <p className="capitalize  text-white text-base">assets</p>
          <div className="flex">
            <div className="bg-blue-500 rounded-full h-8 w-8 shadow-sm z-30"></div>
            <div className="bg-green-500 rounded-full h-8 w-8 shadow-sm -ml-5 z-20"></div>
            <div className="bg-yellow-500 rounded-full h-8 w-8 shadow-sm -ml-5 z-10"></div>
          </div>
        </div>
        <div>
          <p className="capitalize text-white text-base">total liquidity</p>
          <p className="text-white font-bitter font-bold">
            {formatCurrency(props.totalLiquidity)}
          </p>
        </div>
        <div>
          <p className="capitalize font-semibold text-white text-base">
            utilization
          </p>
          <p className="text-white font-bitter font-bold">
            {formatNumber(props.utilization)}%
          </p>
        </div>
        <div>
          <p className="capitalize font-semibold text-white text-base">pnl</p>
          <p className="text-green-300 font-bitter font-bold">
            {formatNumber(props.pnl)}%
          </p>
        </div>
      </div>

      <div className="rounded-b-3xl flex justify-center border-t-2 border-t-white/5 p-5  bg-gradient-to-br from-white/10">
        <button className="text-white font-bitter font-bold uppercase bg-gradient-to-br from-pink-600 to-pink-300 rounded-full px-3 py-2 leading-none">
          click me
        </button>
      </div>
    </div>
  );
};
