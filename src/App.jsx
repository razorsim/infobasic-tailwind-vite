import { CardOne } from "./components/CardOne";

function App() {
  return (
    <div className="font-raleway snap-y snap-mandatory h-screen overflow-y-scroll scroll-smooth ">
      <div className="bg-blue-500 h-screen w-full snap-start relative">
        <img
          src="https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/potw1930a.jpg"
          alt=""
          className="absolute w-screen h-screen object-cover"
        />
        <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-5 p-5 relative">
          <div className="bg-blue-500 p-5 flex justify-center items-center rounded-3xl relative">
            <div>ICON</div>
            <div className="absolute bottom-5">Label</div>
          </div>
          <CardOne
            className="self-start"
            active={false}
            totalLiquidity="29382839"
            utilization="47.2"
            pnl="18.4"
          />
          <CardOne
            className="self-start"
            active={true}
            totalLiquidity="38294"
            utilization="12.2"
            pnl="78.4"
          />
          <CardOne
            className="self-start"
            active={true}
            totalLiquidity="38294"
            utilization="12.2"
            pnl="78.4"
          />
        </div>
      </div>
      <div className="bg-yellow-500 h-screen w-full snap-start"></div>
      <div className="bg-red-500 h-screen w-full snap-start"></div>
    </div>
  );
}

export default App;
